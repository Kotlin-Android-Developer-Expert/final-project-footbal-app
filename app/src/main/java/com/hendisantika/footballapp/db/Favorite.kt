package com.hendisantika.footballapp.db

/**
 * Created by hendisantika on 28/10/18  16.39.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */

data class Favorite(
    val id: Long?,
    val eventId: String?,
    val eventDate: String?,
    val eventTime: String?,
    val homeName: String?,
    val homeScore: String?,
    val awayName: String?,
    val awayScore: String?,
    val teamName: String?,
    val teamBadge: String?,
    val favType: String?
) {

    companion object {
        const val TABLE_FAV: String = "football"
        const val ID: String = "id_"
        const val EVENT_ID: String = "eventId"
        const val EVENT_DATE: String = "eventDate"
        const val EVENT_TIME: String = "eventTime"
        const val HOME_NAME: String = "homeName"
        const val HOME_SCORE: String = "homeScore"
        const val AWAY_NAME: String = "awayName"
        const val AWAY_SCORE: String = "awayScore"
        const val TEAM_NAME: String = "teamName"
        const val TEAM_BADGE: String = "teamBadge"
        const val FAV_TYPE: String = "type"
    }
}