package com.hendisantika.footballapp.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

/**
 * Created by hendisantika on 28/10/18  16.41.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoriteHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MyFavorites.db", null, 1) {

    companion object {
        private var instance: FavoriteHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): FavoriteHelper {
            if (instance == null) {
                instance = FavoriteHelper(ctx.applicationContext)
            }
            return instance as FavoriteHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable(
            Favorite.TABLE_FAV, true,
            Favorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Favorite.EVENT_ID to TEXT + UNIQUE,
            Favorite.EVENT_DATE to TEXT,
            Favorite.EVENT_TIME to TEXT,
            Favorite.HOME_NAME to TEXT,
            Favorite.HOME_SCORE to TEXT,
            Favorite.AWAY_NAME to TEXT,
            Favorite.AWAY_SCORE to TEXT,
            Favorite.TEAM_NAME to TEXT,
            Favorite.TEAM_BADGE to TEXT,
            Favorite.FAV_TYPE to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.dropTable(Favorite.TABLE_FAV, true)
    }
}

val Context.database: FavoriteHelper
    get() = FavoriteHelper.getInstance(applicationContext)