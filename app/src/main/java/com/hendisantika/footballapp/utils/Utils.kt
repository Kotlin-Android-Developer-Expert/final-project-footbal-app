package com.hendisantika.footballapp.utils

import android.annotation.SuppressLint
import android.view.View
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by hendisantika on 28/10/18  16.26.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

@SuppressLint("SimpleDateFormat")
fun formatDate(date: String): String {
    val input = SimpleDateFormat("dd/MM/yy")
    val output = SimpleDateFormat("EEE, dd MMM yyy")
    return output.format(input.parse(date))
}

@SuppressLint("SimpleDateFormat")
fun formatTime(time: String): String {
    val input = SimpleDateFormat("HH:mm:ss")
    input.timeZone = TimeZone.getTimeZone("UTC")
    val output = SimpleDateFormat("HH:mm")
    return output.format(input.parse(time))
}

@SuppressLint("SimpleDateFormat")
fun concateDateTimeZone(date: String, time: String): Long {
    val input = SimpleDateFormat("dd/MM/yy HH:mm:ss")
    input.timeZone = TimeZone.getTimeZone("UTC")
    return input.parse("$date $time").time
}