package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  17.06.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface TeamDetailsView {
    fun showLoading()
    fun hideLoading()
    fun setData(data: List<Model.TeamDetailsData>)
}