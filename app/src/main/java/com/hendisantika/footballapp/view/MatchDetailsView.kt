package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  16.33.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface MatchDetailsView {
    fun showLoading()
    fun hideLoading()
    fun setEventData(data: List<Model.EventDetailsData>)
    fun setTeamBadge(home: String?, away: String?)
}