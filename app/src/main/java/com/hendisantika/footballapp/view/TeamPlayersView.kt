package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  17.27.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface TeamPlayersView {
    fun showLoading()
    fun hideLoading()
    fun showPlayersList(data: List<Model.PlayersData>)
}