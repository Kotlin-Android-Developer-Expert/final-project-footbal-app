package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  17.32.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface PlayerView {
    fun showLoading()
    fun hideLoading()
    fun setPlayerData(data: List<Model.PlayersData>)
}