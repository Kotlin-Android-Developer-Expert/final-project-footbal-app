package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  17.00.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun showTeamsList(data: List<Model.TeamsData>)
    fun setSpinner(leagueName: ArrayList<String>)
}