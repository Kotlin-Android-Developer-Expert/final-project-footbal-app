package com.hendisantika.footballapp.view

import com.hendisantika.footballapp.model.Model

/**
 * Created by hendisantika on 28/10/18  16.15.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface NextMatchView {
    fun showLoading()

    fun hideLoading()

    fun showEventList(data: List<Model.EventData>)

    fun setSpinner(leagueName: ArrayList<String>, leagueId: ArrayList<String>)
}