package com.hendisantika.footballapp.activity

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.adapter.TeamPagerAdapter
import com.hendisantika.footballapp.db.Favorite
import com.hendisantika.footballapp.db.database
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.TeamDetailsPresenter
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.TeamDetailsView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_team_details.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find

/**
 * Created by hendisantika on 28/10/18  17.05.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamDetailsActivity : AppCompatActivity(), TeamDetailsView {

    private lateinit var presenter: TeamDetailsPresenter

    private lateinit var toolbar: Toolbar
    private lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    private lateinit var menuItem: Menu

    private var isFavorite: Boolean = false
    private lateinit var teamBadge: String
    private lateinit var teamName: String

    companion object {
        lateinit var teamDesc: String
        lateinit var teamId: String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_details)

        teamId = intent.getStringExtra("id")

        toolbar = find(R.id.htab_toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Team Details"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        collapsingToolbarLayout = find(R.id.htab_collapse_toolbar)

        presenter = TeamDetailsPresenter(this)
        presenter.getTeamDetails(teamId)

        favoriteState()
    }

    override fun showLoading() {
        pb_td.visible()
    }

    override fun hideLoading() {
        pb_td.gone()
    }

    override fun setData(data: List<Model.TeamDetailsData>) {
        Picasso.get().load(data[0].teamBadge).into(htab_header)
        player_name.text = data[0].strTeam
        team_fy.text = data[0].formedYear
        team_stadion.text = data[0].stadium
        teamDesc = data[0].descriptionEN
        teamBadge = data[0].teamBadge
        teamName = data[0].strTeam

        htab_viewpager.adapter = TeamPagerAdapter(supportFragmentManager)
        htab_tabs.setupWithViewPager(htab_viewpager)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (isFavorite) removeFromFavorite() else addToFavorite()
                isFavorite = !isFavorite
                setFavorite()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAV)
                .whereArgs("(${Favorite.EVENT_ID} = {id})", "id" to teamId)
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(
                    Favorite.TABLE_FAV,
                    Favorite.EVENT_ID to teamId,
                    Favorite.TEAM_NAME to teamName,
                    Favorite.TEAM_BADGE to teamBadge,
                    Favorite.FAV_TYPE to "team"
                )
            }
            snackbar(htab_maincontent, "Added to my favorite!").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(htab_maincontent, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    Favorite.TABLE_FAV, "(${Favorite.EVENT_ID} = {id})",
                    "id" to teamId
                )
            }
            snackbar(htab_maincontent, "Removed from favorite!").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(htab_maincontent, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem.getItem(0)?.icon = ContextCompat.getDrawable(
                this, R.drawable.ic_star
            )
        else
            menuItem.getItem(0)?.icon = ContextCompat.getDrawable(
                this, R.drawable.ic_star_border
            )
    }
}