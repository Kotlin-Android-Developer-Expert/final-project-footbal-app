package com.hendisantika.footballapp.activity

import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.db.Favorite
import com.hendisantika.footballapp.db.database
import com.hendisantika.footballapp.layout.MatchDetailsLayout
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.MatchDetailsPresenter
import com.hendisantika.footballapp.utils.*
import com.hendisantika.footballapp.view.MatchDetailsView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.find
import org.jetbrains.anko.setContentView
import org.jetbrains.anko.support.v4.onRefresh

/**
 * Created by hendisantika on 28/10/18  16.33.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsActivity : AppCompatActivity(), MatchDetailsView {

    private lateinit var presenter: MatchDetailsPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var menuItem: Menu
    private lateinit var eventId: String

    private var isFavorite: Boolean = false
    private var type: Int = 1
    private lateinit var eventDate: String
    private lateinit var eventTime: String
    private lateinit var alertTitle: String
    private lateinit var homeName: String
    private lateinit var homeScore: String
    private lateinit var awayName: String
    private lateinit var awayScore: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MatchDetailsLayout().setContentView(this)

        progressBar = find(R.id.md_progress)
        swipeRefresh = find(R.id.md_swipeRefresh)

        eventId = intent.getStringExtra("id")
        type = intent.getIntExtra("type", 1)

        presenter = MatchDetailsPresenter(this)
        presenter.getEventData(eventId)

        swipeRefresh.onRefresh {
            presenter.getEventData(eventId)
        }

        if (type == 2) {
            favoriteState()
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun setTeamBadge(home: String?, away: String?) {
        swipeRefresh.isRefreshing = false

        val homeImg: ImageView = find(R.id.md_img_home)
        val awayImg: ImageView = find(R.id.md_img_away)

        Picasso.get().load(home).into(homeImg)
        Picasso.get().load(away).into(awayImg)
    }

    override fun setEventData(data: List<Model.EventDetailsData>) {
        eventDate = data[0].strDate
        eventTime = data[0].strTime
        homeName = data[0].homeTeam
        homeScore = data[0].homeScore
        awayName = data[0].awayTeam
        awayScore = data[0].awayScore
        alertTitle = data[0].filename

        val homeName: TextView = find(R.id.md_home_team)
        val homeScore: TextView = find(R.id.md_home_score)
        val homeGoal: TextView = find(R.id.md_home_goal)
        val homeShots: TextView = find(R.id.md_home_shots)
        val homeKeeper: TextView = find(R.id.md_home_keeper)
        val homeDefense: TextView = find(R.id.md_home_defense)
        val homeMid: TextView = find(R.id.md_home_midfield)
        val homeForward: TextView = find(R.id.md_home_forward)
        val homeSubs: TextView = find(R.id.md_home_substitute)

        val awayName: TextView = find(R.id.md_away_team)
        val awayScore: TextView = find(R.id.md_away_score)
        val awayGoal: TextView = find(R.id.md_away_goal)
        val awayShots: TextView = find(R.id.md_away_shots)
        val eventDate: TextView = find(R.id.md_date)
        val eventTime: TextView = find(R.id.md_time)
        val awayKeeper: TextView = find(R.id.md_away_keeper)
        val awayDefense: TextView = find(R.id.md_away_defense)
        val awayMid: TextView = find(R.id.md_away_midfield)
        val awayForward: TextView = find(R.id.md_away_forward)
        val awaySubs: TextView = find(R.id.md_away_substitute)

        homeName.text = data[0].homeTeam
        homeScore.text = data[0].homeScore
        homeGoal.text = setData(data[0].homeGoalDetails)
        homeShots.text = data[0].homeShots

        awayName.text = data[0].awayTeam
        awayScore.text = data[0].awayScore
        awayGoal.text = setData(data[0].awayGoalDetails)
        awayShots.text = data[0].awayShots

        eventDate.text = formatDate(data[0].strDate)
        eventTime.text = formatTime(data[0].strTime)

        homeKeeper.text = setData(data[0].homeLineupGoalkeeper)
        awayKeeper.text = setData(data[0].awayLineupGoalkeeper)

        homeDefense.text = setData(data[0].homeLineupDefense)
        homeMid.text = setData(data[0].homeLineupMidfield)
        homeForward.text = setData(data[0].homeLineupForward)
        homeSubs.text = setData(data[0].homeLineupSubstitutes)

        awayDefense.text = setData(data[0].awayLineupDefense)
        awayMid.text = setData(data[0].awayLineupMidfield)
        awayForward.text = setData(data[0].awayLineupForward)
        awaySubs.text = setData(data[0].awayLineupSubstitutes)
    }

    private fun setData(data: String?): String {
        return data?.replace(";", "\n") ?: "No Data"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.detail_menu, menu)
        menuItem = menu
        if (type == 2) {
            setFavorite()
        } else {
            menuItem.getItem(0)?.icon = ContextCompat.getDrawable(
                this, R.drawable.ic_add_alert
            )
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.add_to_favorite -> {
                if (type == 2) {
                    if (isFavorite) removeFromFavorite() else addToFavorite()

                    isFavorite = !isFavorite
                    setFavorite()
                } else {
                    val i = Intent(Intent.ACTION_EDIT)
                    i.type = "vnd.android.cursor.item/event"
                    i.putExtra("beginTime", concateDateTimeZone(eventDate, eventTime))
                    i.putExtra("eventTime", formatTime(eventTime))
                    i.putExtra("allDay", true)
                    i.putExtra("rule", "FREQ=YEARLY")
                    i.putExtra("endTime", concateDateTimeZone(eventDate, eventTime) + 60 * 60 * 1000)
                    i.putExtra("title", alertTitle)
                    startActivity(i)
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(Favorite.TABLE_FAV)
                .whereArgs("(${Favorite.EVENT_ID} = {id})", "id" to eventId)
            val favorite = result.parseList(classParser<Favorite>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(
                    Favorite.TABLE_FAV,
                    Favorite.EVENT_ID to eventId,
                    Favorite.EVENT_DATE to formatDate(eventDate),
                    Favorite.EVENT_TIME to formatTime(eventTime),
                    Favorite.HOME_NAME to homeName,
                    Favorite.HOME_SCORE to homeScore,
                    Favorite.AWAY_NAME to awayName,
                    Favorite.AWAY_SCORE to awayScore,
                    Favorite.FAV_TYPE to "match"
                )
            }
            snackbar(swipeRefresh, "Added to my favorite").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    Favorite.TABLE_FAV, "(${Favorite.EVENT_ID} = {id})",
                    "id" to eventId
                )
            }
            snackbar(swipeRefresh, "Removed from my favorite!").show()
        } catch (e: SQLiteConstraintException) {
            snackbar(swipeRefresh, e.localizedMessage).show()
        }
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem.getItem(0)?.icon = ContextCompat.getDrawable(
                this, R.drawable.ic_star
            )
        else
            menuItem.getItem(0)?.icon = ContextCompat.getDrawable(
                this, R.drawable.ic_star_border
            )
    }
}