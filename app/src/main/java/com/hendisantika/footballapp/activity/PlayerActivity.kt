package com.hendisantika.footballapp.activity

import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.MenuItem
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.PlayerPresenter
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.PlayerView
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*

/**
 * Created by hendisantika on 28/10/18  17.31.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class PlayerActivity : AppCompatActivity(), PlayerView {

    private lateinit var presenter: PlayerPresenter

    private lateinit var progressBar: ProgressBar
    private lateinit var playerId: String
    private lateinit var playerImg: ImageView
    private lateinit var playerWeight: TextView
    private lateinit var playerheight: TextView
    private lateinit var playerPosition: TextView
    private lateinit var playerInfo: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verticalLayout {
            lparams(width = matchParent, height = matchParent)

            playerImg = imageView {
                scaleType = ImageView.ScaleType.CENTER_CROP
            }.lparams(width = matchParent, height = wrapContent)

            linearLayout {
                lparams(width = matchParent, height = wrapContent) {
                    topMargin = dip(16)
                }
                orientation = LinearLayout.HORIZONTAL

                verticalLayout {
                    lparams(width = 0, height = wrapContent, weight = 1f)
                    textView("Weight (Kg)") {
                        gravity = Gravity.CENTER
                    }
                    playerWeight = textView {
                        textSize = 40F
                        lines = 1
                        textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        gravity = Gravity.CENTER
                    }
                }

                verticalLayout {
                    lparams(width = 0, height = wrapContent, weight = 1f)
                    textView("Height (m)") {
                        gravity = Gravity.CENTER
                    }
                    playerheight = textView {
                        textSize = 40F
                        lines = 1
                        textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        gravity = Gravity.CENTER
                    }
                }
            }

            playerPosition = textView {
                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                typeface = Typeface.DEFAULT_BOLD
            }.lparams {
                topMargin = dip(8)
                gravity = Gravity.CENTER
            }

            view {
                backgroundColor = Color.GRAY
            }.lparams(width = matchParent, height = dip(1)) {
                margin = dip(8)
            }

            scrollView {
                leftPadding = dip(8)
                rightPadding = dip(8)
                bottomPadding = dip(8)
                playerInfo = textView {}
            }

            relativeLayout {
                lparams(width = matchParent, height = matchParent)

                progressBar = progressBar {

                }.lparams(width = wrapContent, height = wrapContent) {
                    centerInParent()
                }
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        playerId = intent.getStringExtra("id")

        presenter = PlayerPresenter(this)
        presenter.getPlayerData(playerId)
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun setPlayerData(data: List<Model.PlayersData>) {
        supportActionBar?.title = data[0].player

        Picasso.get().load(data[0].thumb).into(playerImg)
        playerWeight.text = data[0].weight
        playerheight.text = data[0].height
        playerPosition.text = data[0].position
        playerInfo.text = data[0].descriptionEN
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}