package com.hendisantika.footballapp.model

import com.google.gson.annotations.SerializedName

/**
 * Created by hendisantika on 28/10/18  16.16.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
object Model {
    data class Event(val events: List<EventData> = listOf())
    data class EventData(
        @SerializedName("idEvent") val idEvent: String,
        @SerializedName("strHomeTeam") val homeTeam: String,
        @SerializedName("strAwayTeam") val awayTeam: String,
        @SerializedName("intHomeScore") val homeScore: String,
        @SerializedName("intAwayScore") val awayScore: String,
        @SerializedName("strDate") val eventDate: String,
        @SerializedName("dateEvent") val dateEvent: String,
        @SerializedName("strLeague") val league: String,
        @SerializedName("strFilename") val filename: String,
        @SerializedName("strTime") val eventTime: String
    )

    data class Leagues(val leagues: List<LeaguesData> = listOf())
    data class LeaguesData(
        @SerializedName("idLeague") val idLeague: String,
        @SerializedName("strLeague") val league: String,
        @SerializedName("strSport") val sport: String
    )

    data class Teams(val teams: List<TeamsData> = listOf())
    data class TeamsData(
        @SerializedName("idTeam") val idTeam: String,
        @SerializedName("strTeam") val team: String,
        @SerializedName("strTeamBadge") val teamBadge: String
    )

    data class EventDetails(val events: List<EventDetailsData> = listOf())
    data class EventDetailsData(
        @SerializedName("idEvent") val idEvent: String,
        @SerializedName("strFilename") val filename: String,
        @SerializedName("strHomeTeam") val homeTeam: String,
        @SerializedName("strAwayTeam") val awayTeam: String,
        @SerializedName("intHomeScore") val homeScore: String,
        @SerializedName("intAwayScore") val awayScore: String,
        @SerializedName("strHomeGoalDetails") val homeGoalDetails: String,
        @SerializedName("strHomeRedCards") val homeRedCards: String,
        @SerializedName("strHomeYellowCards") val homeYellowCards: String,
        @SerializedName("strHomeLineupGoalkeeper") val homeLineupGoalkeeper: String,
        @SerializedName("strHomeLineupDefense") val homeLineupDefense: String,
        @SerializedName("strHomeLineupMidfield") val homeLineupMidfield: String,
        @SerializedName("strHomeLineupForward") val homeLineupForward: String,
        @SerializedName("strHomeLineupSubstitutes") val homeLineupSubstitutes: String,
        @SerializedName("strHomeFormation") val homeFormation: String,
        @SerializedName("strAwayRedCards") val awayRedCards: String,
        @SerializedName("strAwayYellowCards") val awayYellowCards: String,
        @SerializedName("strAwayGoalDetails") val awayGoalDetails: String,
        @SerializedName("strAwayLineupGoalkeeper") val awayLineupGoalkeeper: String,
        @SerializedName("strAwayLineupDefense") val awayLineupDefense: String,
        @SerializedName("strAwayLineupMidfield") val awayLineupMidfield: String,
        @SerializedName("strAwayLineupForward") val awayLineupForward: String,
        @SerializedName("strAwayLineupSubstitutes") val awayLineupSubstitutes: String,
        @SerializedName("strAwayFormation") val awayFormation: String,
        @SerializedName("intHomeShots") val homeShots: String,
        @SerializedName("intAwayShots") val awayShots: String,
        @SerializedName("dateEvent") val dateEvent: String,
        @SerializedName("strDate") val strDate: String,
        @SerializedName("strTime") val strTime: String,
        @SerializedName("idHomeTeam") val idHomeTeam: String,
        @SerializedName("idAwayTeam") val idAwayTeam: String
    )

    data class TeamDetails(val teams: List<TeamDetailsData> = listOf())
    data class TeamDetailsData(
        @SerializedName("idTeam") val idTeam: String,
        @SerializedName("strTeam") val strTeam: String,
        @SerializedName("strTeamShort") val teamShort: String,
        @SerializedName("strAlternate") val alternate: String,
        @SerializedName("intFormedYear") val formedYear: String,
        @SerializedName("strSport") val sport: String,
        @SerializedName("strLeague") val league: String,
        @SerializedName("idLeague") val idLeague: String,
        @SerializedName("strManager") val manager: String,
        @SerializedName("strStadium") val stadium: String,
        @SerializedName("strStadiumThumb") val stadiumThumb: String,
        @SerializedName("strStadiumDescription") val stadiumDescription: String,
        @SerializedName("strStadiumLocation") val stadiumLocation: String,
        @SerializedName("intStadiumCapacity") val stadiumCapacity: String,
        @SerializedName("strDescriptionEN") val descriptionEN: String,
        @SerializedName("strTeamBadge") val teamBadge: String,
        @SerializedName("strTeamJersey") val teamJersey: String,
        @SerializedName("strTeamLogo") val teamLogo: String,
        @SerializedName("strTeamBanner") val teamBanner: String
    )

    data class Players(val player: List<PlayersData> = listOf())
    data class PlayerDetails(val players: List<PlayersData> = listOf())
    data class PlayersData(
        @SerializedName("idPlayer") val idPlayer: String,
        @SerializedName("idTeam") val idTeam: String,
        @SerializedName("strNationality") val nationality: String,
        @SerializedName("strPlayer") val player: String,
        @SerializedName("strTeam") val team: String,
        @SerializedName("dateBorn") val born: String,
        @SerializedName("dateSigned") val signed: String,
        @SerializedName("strSigning") val signing: String,
        @SerializedName("strBirthLocation") val birthLocation: String,
        @SerializedName("strDescriptionEN") val descriptionEN: String,
        @SerializedName("strGender") val gender: String,
        @SerializedName("strPosition") val position: String,
        @SerializedName("strHeight") val height: String,
        @SerializedName("strWeight") val weight: String,
        @SerializedName("strThumb") val thumb: String,
        @SerializedName("strCutout") val cutout: String
    )
}