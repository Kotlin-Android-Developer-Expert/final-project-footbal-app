package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.MatchDetailsView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  16.35.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsPresenter(private val view: MatchDetailsView) {

    fun getEventData(eventId: String) {
        view.showLoading()

        SportDbAPI.regular().eventDetails(eventId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setEventData(result.events)
                    getHomeTeamBadge(
                        result.events[0].idHomeTeam,
                        result.events[0].idAwayTeam
                    )
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
    }

    private fun getHomeTeamBadge(homeId: String?, awayId: String?) {
        view.showLoading()

        SportDbAPI.regular().teamDetails(homeId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    getAwayTeamBadge(result.teams[0].teamBadge, awayId)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
    }

    private fun getAwayTeamBadge(homeBadge: String?, awayId: String?) {
        view.showLoading()

        SportDbAPI.regular().teamDetails(awayId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setTeamBadge(homeBadge, result.teams[0].teamBadge)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
    }
}