package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.TeamsView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  17.01.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamsPresenter(private val view: TeamsView) {

    fun getLeagues() {
        view.showLoading()
        SportDbAPI.regular().allLeagues()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    val leagueName = ArrayList<String>()
                    val leagueId = ArrayList<String>()
                    if (result.leagues.isNotEmpty()) {
                        for (i in 0 until result.leagues.size) {
                            if (result.leagues[i].sport == "Soccer") {
                                leagueName.add(result.leagues[i].league)
                                leagueId.add(result.leagues[i].idLeague)
                            }
                        }
                    }
                    view.setSpinner(leagueName)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }

    fun getTeamsList(id: String) {
        view.showLoading()
        SportDbAPI.regular().allTeams(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.showTeamsList(result.teams)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }
}