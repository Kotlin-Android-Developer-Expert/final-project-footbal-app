package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.TeamDetailsView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  17.18.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamDetailsPresenter(private val view: TeamDetailsView) {

    fun getTeamDetails(id: String) {
        view.showLoading()

        SportDbAPI.regular().teamDetails(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setData(result.teams)
                },
                { error ->
                    Log.e("Error", error.message)
                }
            )
    }
}