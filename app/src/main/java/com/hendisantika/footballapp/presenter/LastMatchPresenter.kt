package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.LastMatchView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  16.53.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class LastMatchPresenter(private val view: LastMatchView) {

    fun getLeagues() {
        view.showLoading()
        SportDbAPI.regular().allLeagues()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    val leagueName = ArrayList<String>()
                    val leagueId = ArrayList<String>()
                    if (result.leagues.isNotEmpty()) {
                        for (i in 0 until result.leagues.size) {
                            if (result.leagues[i].sport == "Soccer") {
                                leagueName.add(result.leagues[i].league)
                                leagueId.add(result.leagues[i].idLeague)
                            }
                        }
                    }
                    view.setSpinner(leagueName, leagueId)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }

    fun getEventList(id: Int) {
        view.showLoading()
        SportDbAPI.regular().lastEventList(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.showEventList(result.events)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }
}