package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.TeamPlayersView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  17.29.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamPlayersPresenter(private val view: TeamPlayersView) {

    fun getPlayers(id: String) {
        view.showLoading()
        SportDbAPI.regular().allPlayers(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.showPlayersList(result.player)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }
}