package com.hendisantika.footballapp.presenter

import android.util.Log
import com.hendisantika.footballapp.api.SportDbAPI
import com.hendisantika.footballapp.view.PlayerView
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * Created by hendisantika on 28/10/18  17.32.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class PlayerPresenter(private val view: PlayerView) {

    fun getPlayerData(id: String) {
        view.showLoading()
        SportDbAPI.regular().playerDetails(id)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    view.hideLoading()
                    view.setPlayerData(result.players)
                },
                { error ->
                    view.hideLoading()
                    Log.e("Error", error.message)
                }
            )
    }
}