package com.hendisantika.footballapp.layout

import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.view.Gravity
import android.widget.LinearLayout
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.MatchDetailsActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by hendisantika on 28/10/18  16.37.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchDetailsLayout : AnkoComponent<MatchDetailsActivity> {
    override fun createView(ui: AnkoContext<MatchDetailsActivity>) = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = LinearLayout.VERTICAL

            swipeRefreshLayout {
                id = R.id.md_swipeRefresh

                scrollView {
                    lparams(width = matchParent, height = matchParent)

                    verticalLayout {
                        lparams(width = matchParent, height = matchParent)
                        padding = dip(16)

                        textView {
                            id = R.id.md_date
                            this.gravity = Gravity.CENTER_HORIZONTAL
                            textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        }.lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.md_time
                            this.gravity = Gravity.CENTER_HORIZONTAL
                            textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        }.lparams(width = matchParent, height = wrapContent)

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(16)
                            }

                            progressBar {
                                id = R.id.md_progress
                            }.lparams {
                                centerInParent()
                            }

                            relativeLayout {
                                lparams(width = wrapContent, height = wrapContent)

                                imageView {
                                    id = R.id.md_img_home
                                }.lparams(width = dip(50), height = dip(50)) {
                                    centerHorizontally()
                                }

                                textView {
                                    id = R.id.md_home_team
                                    textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                                }.lparams {
                                    centerHorizontally()
                                    topMargin = dip(16)
                                    below(R.id.md_img_home)
                                }
                            }

                            textView {
                                id = R.id.md_home_score
                                textSize = 32F
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams {
                                centerVertically()
                                leftOf(R.id.md_vs)
                                rightMargin = dip(16)
                            }

                            textView("VS") {
                                id = R.id.md_vs
                                textSize = 16F
                            }.lparams {
                                centerInParent()
                                centerVertically()
                            }

                            textView {
                                id = R.id.md_away_score
                                textSize = 32F
                                typeface = Typeface.DEFAULT_BOLD
                            }.lparams {
                                centerVertically()
                                rightOf(R.id.md_vs)
                                leftMargin = dip(16)
                            }

                            relativeLayout {
                                lparams(width = wrapContent, height = wrapContent)

                                imageView {
                                    id = R.id.md_img_away
                                }.lparams(width = dip(50), height = dip(50)) {
                                    centerHorizontally()
                                }

                                textView {
                                    id = R.id.md_away_team
                                    textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                                }.lparams {
                                    centerHorizontally()
                                    topMargin = dip(16)
                                    below(R.id.md_img_away)
                                }
                            }.lparams {
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_goal)
                                baselineOf(R.id.md_goal)
                            }

                            textView("GOALS") {
                                id = R.id.md_goal
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_goal)
                                baselineOf(R.id.md_goal)
                            }

                            textView {
                                id = R.id.md_home_goal
                            }.lparams {
                                below(R.id.md_goal)
                            }

                            textView {
                                id = R.id.md_away_goal
                            }.lparams {
                                below(R.id.md_goal)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_shots)
                                baselineOf(R.id.md_shots)
                            }

                            textView("SHOTS") {
                                id = R.id.md_shots
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_shots)
                                baselineOf(R.id.md_shots)
                            }

                            textView {
                                id = R.id.md_home_shots
                            }.lparams {
                                below(R.id.md_shots)
                            }

                            textView {
                                id = R.id.md_away_shots
                            }.lparams {
                                below(R.id.md_shots)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_keeper)
                                baselineOf(R.id.md_keeper)
                            }

                            textView("GOAL KEEPER") {
                                id = R.id.md_keeper
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_keeper)
                                baselineOf(R.id.md_keeper)
                            }

                            textView {
                                id = R.id.md_home_keeper
                            }.lparams {
                                below(R.id.md_keeper)
                            }

                            textView {
                                id = R.id.md_away_keeper
                            }.lparams {
                                below(R.id.md_keeper)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_defense)
                                baselineOf(R.id.md_defense)
                            }

                            textView("DEFENSE") {
                                id = R.id.md_defense
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_defense)
                                baselineOf(R.id.md_defense)
                            }

                            textView {
                                id = R.id.md_home_defense
                            }.lparams {
                                below(R.id.md_defense)
                            }

                            textView {
                                id = R.id.md_away_defense
                            }.lparams {
                                below(R.id.md_defense)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_midfield)
                                baselineOf(R.id.md_midfield)
                            }

                            textView("MIDFIELD") {
                                id = R.id.md_midfield
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_midfield)
                                baselineOf(R.id.md_midfield)
                            }

                            textView {
                                id = R.id.md_home_midfield
                            }.lparams {
                                below(R.id.md_midfield)
                            }

                            textView {
                                id = R.id.md_away_midfield
                            }.lparams {
                                below(R.id.md_midfield)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_forward)
                                baselineOf(R.id.md_forward)
                            }

                            textView("FORWARD") {
                                id = R.id.md_forward
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_forward)
                                baselineOf(R.id.md_forward)
                            }

                            textView {
                                id = R.id.md_home_forward
                            }.lparams {
                                below(R.id.md_forward)
                            }

                            textView {
                                id = R.id.md_away_forward
                            }.lparams {
                                below(R.id.md_forward)
                                alignParentRight()
                            }
                        }

                        relativeLayout {
                            lparams(width = matchParent, height = wrapContent) {
                                topMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                leftOf(R.id.md_substitute)
                                baselineOf(R.id.md_substitute)
                            }

                            textView("SUBSTITUTES") {
                                id = R.id.md_substitute
                                textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                            }.lparams {
                                centerHorizontally()
                                leftMargin = dip(8)
                                rightMargin = dip(8)
                            }

                            view {
                                backgroundColor = Color.GRAY
                            }.lparams(width = matchParent, height = dip(1)) {
                                rightOf(R.id.md_substitute)
                                baselineOf(R.id.md_substitute)
                            }

                            textView {
                                id = R.id.md_home_substitute
                            }.lparams {
                                below(R.id.md_substitute)
                            }

                            textView {
                                id = R.id.md_away_substitute
                            }.lparams {
                                below(R.id.md_substitute)
                                alignParentRight()
                            }
                        }
                    }
                }
            }
        }
    }
}