package com.hendisantika.footballapp.adapter

import android.content.Context
import android.graphics.Typeface
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.utils.formatDate
import com.hendisantika.footballapp.utils.formatTime
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * Created by hendisantika on 28/10/18  16.48.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class LastMatchAdapter(
    private val ctx: Context,
    private val data: List<Model.EventData>,
    private var dataFiltered: List<Model.EventData>,
    private val listener: (Model.EventData) -> Unit
) : RecyclerView.Adapter<LastMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LastMatchViewHolder {
        return LastMatchViewHolder(LastMatchtUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = dataFiltered.size

    override fun onBindViewHolder(holder: LastMatchViewHolder, position: Int) {
        holder.bindItem(ctx, dataFiltered[position], listener)
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val query = charSequence.toString()

                var filtered: MutableList<Model.EventData> = mutableListOf()

                if (query.isEmpty()) {
                    filtered = data.toMutableList()
                } else {
                    for (i in data) {
                        if (i.filename.toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(i)
                        }
                    }
                }

                val results = FilterResults()
                results.count = filtered.size
                results.values = filtered
                return results
            }

            override fun publishResults(charSequence: CharSequence, results: FilterResults) {
                dataFiltered = results.values as List<Model.EventData>
                notifyDataSetChanged()
            }
        }
    }
}

class LastMatchtUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                    leftMargin = dip(8)
                    rightMargin = dip(8)
                }

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(8)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.event_date
                            gravity = Gravity.CENTER_HORIZONTAL
                            maxLines = 1
                            textSize = 12f
                            ellipsize = TextUtils.TruncateAt.END
                            textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        }.lparams {
                            centerInParent()
                        }
                    }

                    view {
                        backgroundColor = ContextCompat.getColor(ctx, R.color.divider)
                    }.lparams(width = matchParent, height = dip(1)) {
                        topMargin = dip(8)
                        bottomMargin = dip(8)
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.event_home
                        }.lparams {
                            alignParentLeft()
                        }

                        textView {
                            id = R.id.event_home_score
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            leftOf(R.id.event_vs)
                        }

                        textView("VS") {
                            id = R.id.event_vs
                        }.lparams {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                marginStart = dip(8)
                                marginEnd = dip(8)
                            }
                            centerHorizontally()
                        }

                        textView {
                            id = R.id.event_away_score
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            rightOf(R.id.event_vs)
                        }

                        textView {
                            id = R.id.event_away
                        }.lparams {
                            alignParentRight()
                        }
                    }
                }
            }
        }
    }
}

class LastMatchViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val home: TextView = view.find(R.id.event_home)
    private val away: TextView = view.find(R.id.event_away)
    private val homeScore: TextView = view.find(R.id.event_home_score)
    private val awayScore: TextView = view.find(R.id.event_away_score)
    private val date: TextView = view.find(R.id.event_date)

    fun bindItem(ctx: Context, data: Model.EventData, listener: (Model.EventData) -> Unit) {
        home.text = data.homeTeam
        homeScore.text = data.homeScore
        away.text = data.awayTeam
        awayScore.text = data.awayScore
        date.text = ctx.resources.getString(
            R.string.tgl, formatDate(data.eventDate), formatTime(data.eventTime)
        )

        itemView.setOnClickListener { listener(data) }
    }
}