package com.hendisantika.footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.hendisantika.footballapp.fragment.TeamOverviewFragment
import com.hendisantika.footballapp.fragment.TeamPlayersFragment

/**
 * Created by hendisantika on 28/10/18  17.20.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> TeamOverviewFragment()
            1 -> TeamPlayersFragment()
            else -> null
        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "OVERVIEW"
            1 -> "PLAYERS"
            else -> null
        }
    }
}