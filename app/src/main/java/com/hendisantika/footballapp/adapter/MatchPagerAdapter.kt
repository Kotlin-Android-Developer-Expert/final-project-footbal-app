package com.hendisantika.footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.hendisantika.footballapp.fragment.LastMatchFragment
import com.hendisantika.footballapp.fragment.NextMatchFragment

/**
 * Created by hendisantika on 28/10/18  16.14.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        return if (position == 0) {
            LastMatchFragment()
        } else {
            NextMatchFragment()

        }
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            "LAST"
        } else {
            "NEXT"
        }
    }
}