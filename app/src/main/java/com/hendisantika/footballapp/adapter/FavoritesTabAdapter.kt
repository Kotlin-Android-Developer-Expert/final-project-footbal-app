package com.hendisantika.footballapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.hendisantika.footballapp.fragment.FavoriteMatchFragment
import com.hendisantika.footballapp.fragment.FavoriteTeamFragment

/**
 * Created by hendisantika on 28/10/18  17.39.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoritesTabAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> FavoriteMatchFragment()
            1 -> FavoriteTeamFragment()
            else -> return null
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "MATCHES"
            1 -> "TEAMS"
            else -> return null
        }
    }
}