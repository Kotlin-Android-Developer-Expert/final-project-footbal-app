package com.hendisantika.footballapp.adapter

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.model.Model
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * Created by hendisantika on 28/10/18  17.30.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamPlayersAdapter(
    private val players: List<Model.PlayersData>,
    private val listener: (Model.PlayersData) -> Unit
) : RecyclerView.Adapter<TeamPlayersViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamPlayersViewHolder {
        return TeamPlayersViewHolder(TeamPlayerUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = players.size

    override fun onBindViewHolder(holder: TeamPlayersViewHolder, position: Int) {
        holder.bindItem(players[position], listener)
    }
}

class TeamPlayerUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                    leftMargin = dip(8)
                    rightMargin = dip(8)
                }

                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(8)

                    imageView {
                        id = R.id.player_img
                    }.lparams(width = dip(50), height = dip(50)) {
                        alignParentLeft()
                        centerVertically()
                    }

                    textView {
                        id = R.id.player_name
                    }.lparams {
                        leftMargin = dip(16)
                        rightOf(R.id.player_img)
                        centerVertically()
                    }

                    textView {
                        id = R.id.player_pos
                        textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                    }.lparams {
                        rightMargin = dip(16)
                        alignParentRight()
                        centerVertically()
                    }
                }
            }
        }
    }
}

class TeamPlayersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val playerName: TextView = view.find(R.id.player_name)
    private val playerPos: TextView = view.find(R.id.player_pos)
    private val playerImg: ImageView = view.find(R.id.player_img)

    fun bindItem(data: Model.PlayersData, listener: (Model.PlayersData) -> Unit) {
        playerName.text = data.player
        playerPos.text = data.position
        Picasso.get().load(data.cutout).into(playerImg)

        itemView.setOnClickListener { listener(data) }
    }
}
