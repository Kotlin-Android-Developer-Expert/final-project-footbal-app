package com.hendisantika.footballapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.model.Model
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * Created by hendisantika on 28/10/18  17.02.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamsAdapter(
    private val teams: List<Model.TeamsData>,
    private var dataFiltered: List<Model.TeamsData>,
    private val listener: (Model.TeamsData) -> Unit
) : RecyclerView.Adapter<TeamsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamsViewHolder {
        return TeamsViewHolder(TeamsUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = dataFiltered.size

    override fun onBindViewHolder(holder: TeamsViewHolder, position: Int) {
        holder.bindItem(dataFiltered[position], listener)
    }

    fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val query = charSequence.toString()

                var filtered: MutableList<Model.TeamsData> = mutableListOf()

                if (query.isEmpty()) {
                    filtered = teams.toMutableList()
                } else {
                    for (i in teams) {
                        if (i.team.toLowerCase().contains(query.toLowerCase())) {
                            filtered.add(i)
                        }
                    }
                }

                val results = FilterResults()
                results.count = filtered.size
                results.values = filtered
                return results
            }

            override fun publishResults(charSequence: CharSequence, results: FilterResults) {
                dataFiltered = results.values as List<Model.TeamsData>
                notifyDataSetChanged()
            }
        }
    }
}

class TeamsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val teamName: TextView = view.find(R.id.team_list_name)
    private val teamBadge: ImageView = view.find(R.id.team_list_img)

    fun bindItem(data: Model.TeamsData, listener: (Model.TeamsData) -> Unit) {
        teamName.text = data.team
        Picasso.get().load(data.teamBadge).into(teamBadge)

        itemView.setOnClickListener { listener(data) }
    }
}

class TeamsUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                    leftMargin = dip(8)
                    rightMargin = dip(8)
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER_VERTICAL
                    padding = dip(8)

                    imageView {
                        id = R.id.team_list_img
                    }.lparams(width = dip(50), height = dip(50))

                    textView {
                        id = R.id.team_list_name
                    }.lparams {
                        leftMargin = dip(16)
                    }
                }
            }
        }
    }
}
