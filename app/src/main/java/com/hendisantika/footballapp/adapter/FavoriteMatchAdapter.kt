package com.hendisantika.footballapp.adapter

import android.graphics.Typeface
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.db.Favorite
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by hendisantika on 28/10/18  17.40.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoriteMatchAdapter(
    private val favorite: List<Favorite>,
    private val listener: (Favorite) -> Unit
) : RecyclerView.Adapter<FavoriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder(FavoriteMatchUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        holder.bindItem(favorite[position], listener)
    }
}

class FavoriteMatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                    leftMargin = dip(8)
                    rightMargin = dip(8)
                }

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(8)

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.fav_match_date
                            gravity = Gravity.CENTER_HORIZONTAL
                            maxLines = 1
                            textSize = 12f
                            ellipsize = TextUtils.TruncateAt.END
                            textColor = ContextCompat.getColor(ctx, R.color.colorAccent)
                        }.lparams {
                            centerInParent()
                        }
                    }

                    view {
                        backgroundColor = ContextCompat.getColor(ctx, R.color.divider)
                    }.lparams(width = matchParent, height = dip(1)) {
                        topMargin = dip(8)
                        bottomMargin = dip(8)
                    }

                    relativeLayout {
                        lparams(width = matchParent, height = wrapContent)

                        textView {
                            id = R.id.fav_match_home
                        }.lparams {
                            alignParentLeft()
                        }

                        textView {
                            id = R.id.fav_match_home_score
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            leftOf(R.id.fav_match_vs)
                        }

                        textView("VS") {
                            id = R.id.fav_match_vs
                        }.lparams {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                                marginStart = dip(8)
                                marginEnd = dip(8)
                            }
                            centerHorizontally()
                        }

                        textView {
                            id = R.id.fav_match_away_score
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            rightOf(R.id.fav_match_vs)
                        }

                        textView {
                            id = R.id.fav_match_away
                        }.lparams {
                            alignParentRight()
                        }
                    }
                }
            }
        }
    }
}

class FavoriteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val home: TextView = view.find(R.id.fav_match_home)
    private val away: TextView = view.find(R.id.fav_match_away)
    private val homeScore: TextView = view.find(R.id.fav_match_home_score)
    private val awayScore: TextView = view.find(R.id.fav_match_away_score)
    private val date: TextView = view.find(R.id.fav_match_date)

    fun bindItem(favorite: Favorite, listener: (Favorite) -> Unit) {
        date.text = favorite.eventDate
        home.text = favorite.homeName
        homeScore.text = favorite.homeScore
        away.text = favorite.awayName
        awayScore.text = favorite.awayScore

        itemView.onClick { listener(favorite) }
    }
}