package com.hendisantika.footballapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.db.Favorite
import com.squareup.picasso.Picasso
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

/**
 * Created by hendisantika on 28/10/18  17.43.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoriteTeamAdapter(
    private val teams: List<Favorite>,
    private val listener: (Favorite) -> Unit
) : RecyclerView.Adapter<TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        return TeamViewHolder(FavoriteTeamUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(teams[position], listener)
    }
}

class FavoriteTeamUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            cardView {
                lparams(width = matchParent, height = wrapContent) {
                    bottomMargin = dip(8)
                    leftMargin = dip(8)
                    rightMargin = dip(8)
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER_VERTICAL
                    padding = dip(8)

                    imageView {
                        id = R.id.fav_team_img
                    }.lparams(width = dip(50), height = dip(50))

                    textView {
                        id = R.id.fav_team_name
                    }.lparams {
                        leftMargin = dip(16)
                    }
                }
            }
        }
    }
}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val teamName: TextView = view.find(R.id.fav_team_name)
    private val teamBadge: ImageView = view.find(R.id.fav_team_img)

    fun bindItem(data: Favorite, listener: (Favorite) -> Unit) {
        teamName.text = data.teamName
        Picasso.get().load(data.teamBadge).into(teamBadge)

        itemView.setOnClickListener { listener(data) }
    }
}