package com.hendisantika.footballapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.PlayerActivity
import com.hendisantika.footballapp.activity.TeamDetailsActivity.Companion.teamId
import com.hendisantika.footballapp.adapter.TeamPlayersAdapter
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.TeamPlayersPresenter
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.TeamPlayersView
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by hendisantika on 28/10/18  17.27.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamPlayersFragment : Fragment(), AnkoComponent<Context>, TeamPlayersView {

    private var playerList: MutableList<Model.PlayersData> = mutableListOf()
    private lateinit var presenter: TeamPlayersPresenter
    private lateinit var adapter: TeamPlayersAdapter

    private lateinit var rvPlayer: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        presenter = TeamPlayersPresenter(this)
        presenter.getPlayers(teamId)

        adapter = TeamPlayersAdapter(playerList) {
            ctx.startActivity<PlayerActivity>("id" to it.idPlayer)
        }
        rvPlayer.adapter = adapter

        swipeRefresh.onRefresh {
            presenter.getPlayers(teamId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent) {
                topPadding = dip(8)
            }
            orientation = android.widget.LinearLayout.VERTICAL

            swipeRefresh = swipeRefreshLayout {
                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    rvPlayer = recyclerView {
                        id = R.id.rv_team_player
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun showPlayersList(data: List<Model.PlayersData>) {
        swipeRefresh.isRefreshing = false
        playerList.clear()
        playerList.addAll(data)
        adapter.notifyDataSetChanged()
    }
}