package com.hendisantika.footballapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hendisantika.footballapp.activity.TeamDetailsActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.ctx

/**
 * Created by hendisantika on 28/10/18  17.21.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamOverviewFragment : Fragment(), AnkoComponent<Context> {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        scrollView {
            lparams(width = matchParent, height = wrapContent)

            textView {
                padding = dip(8)
                text = TeamDetailsActivity.teamDesc
            }
        }
    }
}