package com.hendisantika.footballapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.MatchDetailsActivity
import com.hendisantika.footballapp.adapter.FavoriteMatchAdapter
import com.hendisantika.footballapp.db.Favorite
import com.hendisantika.footballapp.db.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by hendisantika on 28/10/18  17.39.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoriteMatchFragment : Fragment(), AnkoComponent<Context> {

    private var favorites: MutableList<Favorite> = mutableListOf()
    private lateinit var adapter: FavoriteMatchAdapter

    private lateinit var listMatch: RecyclerView
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = FavoriteMatchAdapter(favorites) {
            ctx.startActivity<MatchDetailsActivity>("id" to "${it.eventId}", "type" to 2)
        }

        listMatch.adapter = adapter
        swipeRefresh.onRefresh {
            showFavorite()
        }

        showFavorite()
    }

    override fun onResume() {
        super.onResume()
        showFavorite()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            topPadding = dip(8)

            swipeRefresh = swipeRefreshLayout {

                listMatch = recyclerView {
                    id = R.id.rv_fav_match
                    lparams(width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }
            }
        }
    }

    private fun showFavorite() {
        favorites.clear()
        context?.database?.use {
            swipeRefresh.isRefreshing = false
            val result = select(Favorite.TABLE_FAV)
                .whereSimple("${Favorite.FAV_TYPE} = ?", "match")
            val favorite = result.parseList(classParser<Favorite>())
            favorites.addAll(favorite)
            adapter.notifyDataSetChanged()
        }
    }
}