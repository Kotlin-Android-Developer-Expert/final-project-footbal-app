package com.hendisantika.footballapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.adapter.FavoritesTabAdapter
import kotlinx.android.synthetic.main.fragment_favorites.*

/**
 * Created by hendisantika on 28/10/18  17.38.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class FavoritesFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewpager_fav.adapter = FavoritesTabAdapter(childFragmentManager)
        tabs_fav.setupWithViewPager(viewpager_fav)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )
            : View? = inflater.inflate(R.layout.fragment_favorites, container, false)
}