package com.hendisantika.footballapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.TeamDetailsActivity
import com.hendisantika.footballapp.adapter.TeamsAdapter
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.TeamsPresenter
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.TeamsView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.coroutines.onClose
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import java.util.*

/**
 * Created by hendisantika on 28/10/18  17.00.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class TeamsFragment : Fragment(), AnkoComponent<Context>, TeamsView {

    private var teamList: MutableList<Model.TeamsData> = mutableListOf()
    private lateinit var presenter: TeamsPresenter
    private lateinit var adapter: TeamsAdapter

    private lateinit var spinner: Spinner
    private lateinit var rvTeams: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = TeamsPresenter(this)
        presenter.getLeagues()

        swipeRefresh.onRefresh {
            presenter.getLeagues()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = android.widget.LinearLayout.VERTICAL

            spinner = spinner {
                id = R.id.sp_team
            }
            swipeRefresh = swipeRefreshLayout {
                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    rvTeams = recyclerView {
                        id = R.id.rv_team
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun setSpinner(leagueName: ArrayList<String>) {
//        val sAdapter = ArrayAdapter(ctx, R.layout.simple_spinner_dropdown_item, leagueName)
        val sAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item, leagueName)
        spinner.adapter = sAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                presenter.getTeamsList(leagueName[pos])
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun showTeamsList(data: List<Model.TeamsData>) {
        swipeRefresh.isRefreshing = false
        teamList.clear()
        teamList.addAll(data)
        adapter = TeamsAdapter(teamList, teamList) {
            ctx.startActivity<TeamDetailsActivity>("id" to it.idTeam)
        }
        rvTeams.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.team_menu, menu)
        val search = menu.findItem(R.id.search_team)

        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search Team"
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                adapter.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        searchView.onClose {
            presenter.getLeagues()
        }
    }
}