package com.hendisantika.footballapp.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.adapter.MatchPagerAdapter
import kotlinx.android.synthetic.main.fragment_matches.*

/**
 * Created by hendisantika on 28/10/18  16.13.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class MatchesFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        vp_match.adapter = MatchPagerAdapter(childFragmentManager)
        tab_match.setupWithViewPager(vp_match)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )
            : View? = inflater.inflate(R.layout.fragment_matches, container, false)
}