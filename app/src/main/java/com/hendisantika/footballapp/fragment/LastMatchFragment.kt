package com.hendisantika.footballapp.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.MatchDetailsActivity
import com.hendisantika.footballapp.adapter.LastMatchAdapter
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.LastMatchPresenter
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.LastMatchView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.coroutines.onClose
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by hendisantika on 28/10/18  16.47.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class LastMatchFragment : Fragment(), AnkoComponent<Context>, LastMatchView {

    private var eventList: MutableList<Model.EventData> = mutableListOf()
    private lateinit var adapter: LastMatchAdapter
    private lateinit var presenter: LastMatchPresenter
    private lateinit var spinner: Spinner
    private lateinit var rvEvents: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = LastMatchPresenter(this)
        presenter.getLeagues()

        swipeRefresh.onRefresh {
            presenter.getLeagues()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = android.widget.LinearLayout.VERTICAL

            spinner = spinner {
                id = R.id.sp_last_match
            }
            swipeRefresh = swipeRefreshLayout {
                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    rvEvents = recyclerView {
                        id = R.id.rv_last_match
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun setSpinner(leagueName: ArrayList<String>, leagueId: ArrayList<String>) {
        val sAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item, leagueName)
        spinner.adapter = sAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                presenter.getEventList(leagueId[pos].toInt())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun showEventList(data: List<Model.EventData>) {
        swipeRefresh.isRefreshing = false
        eventList.clear()
        eventList.addAll(data)
        adapter = LastMatchAdapter(ctx, eventList, eventList) {
            ctx.startActivity<MatchDetailsActivity>("id" to it.idEvent, "type" to 2)
        }
        rvEvents.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.matches_menu, menu)
        val search = menu.findItem(R.id.search_match)

        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search Last Match"
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                adapter.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        searchView.onClose {
            presenter.getLeagues()
        }
    }
}