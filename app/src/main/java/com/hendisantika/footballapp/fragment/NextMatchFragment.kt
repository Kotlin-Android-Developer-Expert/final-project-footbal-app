package com.hendisantika.footballapp.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import com.hendisantika.footballapp.R
import com.hendisantika.footballapp.activity.MatchDetailsActivity
import com.hendisantika.footballapp.adapter.NextMatchAdapter
import com.hendisantika.footballapp.model.Model
import com.hendisantika.footballapp.presenter.NextMatchPresenter
import com.hendisantika.footballapp.utils.concateDateTimeZone
import com.hendisantika.footballapp.utils.gone
import com.hendisantika.footballapp.utils.visible
import com.hendisantika.footballapp.view.NextMatchView
import org.jetbrains.anko.*
import org.jetbrains.anko.appcompat.v7.coroutines.onClose
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout
import java.util.*

/**
 * Created by hendisantika on 28/10/18  16.15.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class NextMatchFragment : Fragment(), AnkoComponent<Context>, NextMatchView {

    private var eventList: MutableList<Model.EventData> = mutableListOf()
    private lateinit var presenter: NextMatchPresenter
    private lateinit var adapter: NextMatchAdapter

    private lateinit var spinner: Spinner
    private lateinit var rvEvents: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = NextMatchPresenter(this)
        presenter.getLeagues()

        swipeRefresh.onRefresh {
            presenter.getLeagues()
        }
    }

    private fun addEvent(data: Model.EventData) {
        val i = Intent(Intent.ACTION_EDIT)
        i.type = "vnd.android.cursor.item/event"
        i.putExtra("beginTime", concateDateTimeZone(data.eventDate, data.eventTime))
        i.putExtra("allDay", true)
        i.putExtra("rule", "FREQ=YEARLY")
        i.putExtra("endTime", concateDateTimeZone(data.eventDate, data.eventTime) + 60 * 60 * 1000)
        i.putExtra("title", data.filename)
        startActivity(i)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            orientation = android.widget.LinearLayout.VERTICAL

            spinner = spinner {
                id = R.id.sp_next_match
            }
            swipeRefresh = swipeRefreshLayout {
                relativeLayout {
                    lparams(width = matchParent, height = wrapContent)

                    rvEvents = recyclerView {
                        id = R.id.rv_next_match
                        lparams(width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }
            }
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.gone()
    }

    override fun setSpinner(leagueName: ArrayList<String>, leagueId: ArrayList<String>) {
        val sAdapter = ArrayAdapter(ctx, R.layout.support_simple_spinner_dropdown_item, leagueName)
        spinner.adapter = sAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
                presenter.getEventList(leagueId[pos].toInt())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun showEventList(data: List<Model.EventData>) {
        swipeRefresh.isRefreshing = false
        eventList.clear()
        eventList.addAll(data)
        adapter = NextMatchAdapter(ctx, eventList, eventList, {
            ctx.startActivity<MatchDetailsActivity>("id" to it.idEvent, "type" to 1)
        }, {
            addEvent(it)
        })
        rvEvents.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.matches_menu, menu)
        val search = menu.findItem(R.id.search_match)

        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search Next Match"
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                adapter.getFilter().filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }
        })
        searchView.onClose {
            presenter.getLeagues()
        }
    }
}