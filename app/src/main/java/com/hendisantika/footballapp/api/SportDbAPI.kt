package com.hendisantika.footballapp.api

import com.hendisantika.footballapp.BuildConfig
import com.hendisantika.footballapp.BuildConfig.DEBUG
import com.hendisantika.footballapp.model.Model
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * Created by hendisantika on 28/10/18  16.20.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
interface SportDbAPI {

    @GET("eventsnextleague.php")
    fun nextEventList(@Query("id") id: Int?): Observable<Model.Event>

    @GET("eventspastleague.php")
    fun lastEventList(@Query("id") id: Int?): Observable<Model.Event>

    @GET("all_leagues.php")
    fun allLeagues(): Observable<Model.Leagues>

    @GET("search_all_teams.php")
    fun allTeams(@Query("l") l: String?): Observable<Model.Teams>

    @GET("lookupevent.php")
    fun eventDetails(@Query("id") id: String?): Observable<Model.EventDetails>

    @GET("lookupteam.php")
    fun teamDetails(@Query("id") id: String?): Observable<Model.TeamDetails>

    @GET("lookup_all_players.php")
    fun allPlayers(@Query("id") id: String?): Observable<Model.Players>

    @GET("lookupplayer.php")
    fun playerDetails(@Query("id") id: String?): Observable<Model.PlayerDetails>

    companion object {
        fun regular(): SportDbAPI {
            val httpClient = OkHttpClient().newBuilder()

            if (DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                httpClient.addInterceptor(loggingInterceptor)
            }

            val retrofit = Retrofit.Builder()
                .client(httpClient.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build()
            return retrofit.create(SportDbAPI::class.java)
        }
    }
}