package com.hendisantika.footballapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.hendisantika.footballapp.fragment.FavoritesFragment
import com.hendisantika.footballapp.fragment.MatchesFragment
import com.hendisantika.footballapp.fragment.TeamsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.nav_matches -> {
                    supportActionBar?.title = "Match Schedule"
                    showFragment(MatchesFragment())
                    true
                }
                R.id.nav_teams -> {
                    supportActionBar?.title = "Football Teams"
                    showFragment(TeamsFragment())
                    true
                }
                R.id.nav_favorites -> {
                    supportActionBar?.title = "My Favorites"
                    showFragment(FavoritesFragment())
                    true
                }
                else -> false
            }
        }
        navigationView.selectedItemId = R.id.nav_matches
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.con_container, fragment)
            .commit()
    }
}
